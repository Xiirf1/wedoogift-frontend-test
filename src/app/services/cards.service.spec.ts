import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { CardsService } from './cards.service';
import {
  HttpClient,
  HttpErrorResponse,
  HttpResponse,
} from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { CardsServiceValue } from 'src/models/calculator.models';

describe('CardsService', () => {
  let service: CardsService;

  let mockHttp: HttpClient;

  let body = {
    equal: {
      value: 40,
      cards: [20, 20],
    },
    floor: {
      value: 42,
      cards: [22, 20],
    },
    ceil: {
      value: 45,
      cards: [22, 23],
    },
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(CardsService);
    mockHttp = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return equal, floor and ceil (without option)', async () => {
    spyOn(mockHttp, 'get').and.returnValue(of(body));

    await service
      .getCardsList(42, 5)
      .then(({ equal, floor, ceil }: CardsServiceValue) => {
        expect(equal).toEqual(body.equal);
        expect(floor).toEqual(body.floor);
        expect(ceil).toEqual(body.ceil);
      });
  });

  it('should return equal with ceil value, floor and ceil (with option plus)', async () => {
    spyOn(mockHttp, 'get').and.returnValue(of(body));

    await service
      .getCardsList(42, 5, 'plus')
      .then(({ equal, floor, ceil }: CardsServiceValue) => {
        expect(equal).toEqual(body.ceil);
        expect(floor).toEqual(body.floor);
        expect(ceil).toEqual(body.ceil);
      });
  });

  it('should return equal with ceil value, floor and ceil (with option plus)', async () => {
    spyOn(mockHttp, 'get').and.returnValue(of(body));

    await service
      .getCardsList(42, 5, 'minus')
      .then(({ equal, floor, ceil }: CardsServiceValue) => {
        expect(equal).toEqual(body.floor);
        expect(floor).toEqual(body.floor);
        expect(ceil).toEqual(body.ceil);
      });
  });

  it('should return error', async () => {
    const error = { message: 'test' };
    const httpResponse = new HttpErrorResponse({
      status: 403,
      error: error,
    });
    spyOn(mockHttp, 'get').and.returnValue(throwError(httpResponse));

    await service.getCardsList(42, 5, 'minus').catch((message) => {
      expect(message).toEqual(error.message);
    });
  });
});
