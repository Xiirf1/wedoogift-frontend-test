import { CardsServiceValue } from './../../models/calculator.models';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

/**
 * Service permettant d'accéder à l'API Calculator
 */
@Injectable({
  providedIn: 'root',
})
export class CardsService {
  constructor(private http: HttpClient) {}

  /**
   * Fonction permettant d'obtenir une liste de carte
   * @param amount somme total des cartes
   * @param shopId id shop de l'utilisateur
   * @param option (optionnel) option de récupération des cartes. minus pour avoir la liste correspondante au montant inférieur, plus pour la liste du montant supérieur et null pour une recherche classique
   * @returns Promise<CardsServiceValue>
   */
  getCardsList(
    amount: number,
    shopId: number,
    option?: string
  ): Promise<CardsServiceValue> {
    if (option) {
      amount = option === 'minus' ? amount - 1 : amount + 1;
    }
    return new Promise((resolve, reject) => {
      this.getCards(amount, shopId).subscribe(
        ({ equal, floor, ceil }: CardsServiceValue) => {
          if (option) {
            option === 'minus'
              ? resolve({ equal: floor, floor, ceil })
              : resolve({ equal: ceil, floor, ceil });
          } else {
            resolve({ equal, floor, ceil });
          }
        },
        (error: HttpErrorResponse) => {
          reject(error.error.message);
        }
      );
    });
  }

  public getCards(amount: number, shopId: number): Observable<any> {
    const cardsServiceUrl = `http://localhost:3000/shop/${shopId}/search-combination`;
    return this.http.get(cardsServiceUrl, { params: { amount } });
  }
}
