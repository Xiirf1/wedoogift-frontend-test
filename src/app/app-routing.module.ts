import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalculatorPagesComponent } from './pages/calculator-pages/calculator-pages.component';

const routes: Routes = [{ path: '', component: CalculatorPagesComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
