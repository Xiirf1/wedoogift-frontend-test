import { Component, OnInit } from '@angular/core';

/**
 * Page du Calculator permettant d'afficher le composant Calculator
 */
@Component({
  selector: 'app-calculator-pages',
  templateUrl: './calculator-pages.component.html',
  styleUrls: ['./calculator-pages.component.scss'],
})
export class CalculatorPagesComponent implements OnInit {
  /**
   * Montant saisie dans le composant fils CalculatorComponent
   */
  public amount = 0;

  constructor() {}

  ngOnInit(): void {}

  /**
   * Fonction permettant de set le montant
   * @param amount montant
   */
  setAmount(amount: number) {
    this.amount = amount;
  }
}
