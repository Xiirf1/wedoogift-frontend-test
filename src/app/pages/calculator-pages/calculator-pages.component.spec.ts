import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { CalculatorPagesComponent } from './calculator-pages.component';

describe('CalculatorPagesComponent', () => {
  let component: CalculatorPagesComponent;
  let fixture: ComponentFixture<CalculatorPagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [CalculatorPagesComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculatorPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set amount', () => {
    component.setAmount(20);
    expect(component.amount).toEqual(20);
  });
});
