import {
  CalculatorComponentValue,
  CardsServiceValue,
} from './../../../models/calculator.models';
import { CardsService } from './../../services/cards.service';
import { Component } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { environment } from 'src/environments/environment';
import { Output, EventEmitter } from '@angular/core';
import { debounceTime } from 'rxjs/operators/';

/**
 * Composant permettant de choisir le montant et d'afficher les cartes correspondantes
 */
@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss'],
})
export class CalculatorComponent {
  /**
   * Objet contenant les champs du formulaires
   */
  public amountForm: FormGroup;

  /**
   * Variable d'erreur, vide si aucune erreur
   */
  public serviceError: string = '';

  /**
   * Liste de carte et montant pour le montant 'inférieur'
   */
  public floor: CalculatorComponentValue = {
    value: 0,
    cards: [],
  };

  /**
   * Liste de carte et montant pour le montant 'supérieur'
   */
  public ceil: CalculatorComponentValue = {
    value: 0,
    cards: [],
  };

  /**
   * Dernier montant, permet de controler le clique sur les boutons suivant et précédent
   */
  public lastAmount: number = 0;

  /**
   * Option de récupération des données (Vide | plus | minus)
   */
  public option: string = '';

  /**
   * Emetteur du montant au composant parent
   */
  @Output() amountEmitter = new EventEmitter<number>();

  constructor(private cardsService: CardsService, private fb: FormBuilder) {
    this.amountForm = this.fb.group({
      amount: [0, [Validators.required, Validators.pattern('^[0-9]*$')]],
      cards: this.fb.array([]),
    });

    this.amountForm
      .get('amount')
      ?.valueChanges.pipe(debounceTime(500))
      .subscribe((changes) => {
        if (this.amountForm.valid) {
          this.amountEmitter.emit(this.amountForm.get('amount')?.value);
        }
      });
  }

  /**
   * Accesseur au tableau de carte du formulaire
   */
  get cards(): FormArray {
    return this.amountForm.get('cards') as FormArray;
  }

  /**
   * Récupération de la liste de carte pour le montant sélectionné
   * @param option Option de récupération des données (Vide | plus | minus)
   */
  async getCards(option?: string) {
    this.lastAmount = this.amountForm.get('amount')?.value;
    this.option = option ? option : '';
    await this.cardsService
      .getCardsList(
        this.amountForm.get('amount')?.value,
        environment.defaultShopId,
        option
      )
      .then(({ equal, floor, ceil }: CardsServiceValue) => {
        if (equal) {
          this.setCard(equal);
        } else {
          this.setMinusPlus(floor, ceil);
        }
      })
      .catch((error: string) => {
        this.setError(error);
      });
  }

  /**
   * Setteur du montant et des cartes
   * @param equal
   */
  setCard(equal: CalculatorComponentValue) {
    this.setMinusPlus();
    this.amountForm.get('amount')?.setValue(equal.value);
    this.cards.clear();
    equal.cards.forEach((card) => {
      this.cards.push(new FormControl(card, Validators.pattern('^[0-9]*$')));
    });
  }

  /**
   * Fonction permettant de set les variables floor et ceil
   * et de mettre en place le level 1.3
   * @param floor
   * @param ceil
   */
  setMinusPlus(
    floor?: CalculatorComponentValue,
    ceil?: CalculatorComponentValue
  ) {
    this.cards.clear();
    this.floor = floor
      ? floor
      : {
          value: 0,
          cards: [],
        };
    this.ceil = ceil
      ? ceil
      : {
          value: 0,
          cards: [],
        };
    if (
      (this.floor?.value === 0 || this.ceil?.value === 0) &&
      (this.floor?.value !== 0 || this.ceil?.value !== 0)
    ) {
      this.amountForm
        .get('amount')
        ?.setValue(this.floor.value === 0 ? this.ceil.value : this.floor.value);
      this.setCard(this.floor.value === 0 ? this.ceil : this.floor);
    }
  }

  /**
   * Setter de l'erreur
   * @param error message d'erreur
   */
  setError(error: string) {
    this.cards.clear();
    this.serviceError = error;
  }

  /**
   * Reset de la variable option
   * Permet de ne pas bloquer l'un des boutons suivants ou précédent en cas d'écriture d'un nouveau montant
   */
  resetOption() {
    this.option = '';
  }
}
