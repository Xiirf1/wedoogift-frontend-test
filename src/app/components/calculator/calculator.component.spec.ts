import { CardsService } from './../../services/cards.service';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { CalculatorComponent } from './calculator.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('CalculatorComponent', () => {
  let component: CalculatorComponent;
  let fixture: ComponentFixture<CalculatorComponent>;
  let mockCardsService: CardsService;

  let body = {
    equal: {
      value: 40,
      cards: [20, 20],
    },
    floor: {
      value: 42,
      cards: [22, 20],
    },
    ceil: {
      value: 45,
      cards: [22, 23],
    },
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [HttpClientTestingModule, FormsModule, ReactiveFormsModule],
      declarations: [CalculatorComponent],
    }).compileComponents();

    mockCardsService = TestBed.inject(CardsService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should reset option', () => {
    component.option = 'test';
    expect(component.option).toEqual('test');
    component.resetOption();
    expect(component.option).toEqual('');
  });

  it('should set error', () => {
    expect(component.serviceError).toEqual('');
    component.setError('testError');
    expect(component.serviceError).toEqual('testError');
  });

  it('should set cards from service', async () => {
    spyOn(mockCardsService, 'getCardsList').and.returnValue(
      Promise.resolve(body)
    );

    await component.getCards();
    expect(component.amountForm.get('amount')?.value).toEqual(40);
    expect(component.cards.controls[0].value).toEqual(20);
    expect(component.cards.controls[1].value).toEqual(20);
  });

  it('should set error from service', async () => {
    spyOn(mockCardsService, 'getCardsList').and.callFake(() =>
      Promise.reject('testError')
    );
    expect(component.serviceError).toEqual('');
    await component.getCards();
    expect(component.serviceError).toEqual('testError');
  });

  it('should set floor and ceil using setMinusPlus function from service', async () => {
    const body = {
      equal: undefined,
      floor: {
        value: 42,
        cards: [22, 20],
      },
      ceil: {
        value: 45,
        cards: [22, 23],
      },
    };
    spyOn(mockCardsService, 'getCardsList').and.returnValue(
      Promise.resolve(body)
    );

    await component.getCards();
    expect(component.amountForm.get('amount')?.value).toEqual(0);
    expect(component.floor).toEqual(body.floor);
    expect(component.ceil).toEqual(body.ceil);
  });

  it('should set cards from function', () => {
    component.setCard({
      value: 40,
      cards: [25, 15],
    });

    expect(component.amountForm.get('amount')?.value).toEqual(40);
    expect(component.cards.controls[0].value).toEqual(25);
    expect(component.cards.controls[1].value).toEqual(15);
  });

  it('should set card using setMinusPlus', () => {
    const floor = {
      value: 0,
      cards: [],
    };
    const ceil = {
      value: 45,
      cards: [22, 23],
    };

    component.setMinusPlus(floor, ceil);

    expect(component.amountForm.get('amount')?.value).toEqual(45);
    expect(component.cards.controls[0].value).toEqual(22);
    expect(component.cards.controls[1].value).toEqual(23);

    const floor2 = {
      value: 43,
      cards: [20, 23],
    };
    const ceil2 = {
      value: 0,
      cards: [],
    };

    component.setMinusPlus(floor2, ceil2);

    expect(component.amountForm.get('amount')?.value).toEqual(43);
    expect(component.cards.controls[0].value).toEqual(20);
    expect(component.cards.controls[1].value).toEqual(23);
  });

  it('should emit amount', fakeAsync(() => {
    spyOn(component.amountEmitter, 'emit');
    component.amountForm.get('amount')?.setValue(30);
    tick(2000);
    expect(component.amountEmitter.emit).toHaveBeenCalled();
    expect(component.amountEmitter.emit).toHaveBeenCalledWith(30);
  }));

  it('should not emit amount', fakeAsync(() => {
    spyOn(component.amountEmitter, 'emit');
    component.amountForm.get('amount')?.setValue('test');
    tick(2000);
    expect(component.amountEmitter.emit).not.toHaveBeenCalled();
  }));
});
