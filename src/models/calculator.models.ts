export interface CalculatorComponentValue {
  value: number;
  cards: number[];
}

export interface CardsServiceValue {
  equal: CalculatorComponentValue | undefined;
  floor: CalculatorComponentValue | undefined;
  ceil: CalculatorComponentValue | undefined;
}
