# WedoogiftFrontendTest

Practice test for Wedoogift by Flavien COSSU

## Pre-requisites - Version
Node : 16.10.0
Angular : 12.2.14

## Installation

Run `npm i`

## Start application

Run `npm start`

## Test

Run `npm test`
Open /coverage/index.html for more informations about test coverage
